﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hacker : MonoBehaviour
{

    //Game Configuartion Data
    [SerializeField] private const string _greeting = "Alright butt";
    [SerializeField] private const string _menuHint = "Type 'menu' to return to main menu";
    [SerializeField] private string[] level1Passwords = { "pizza", "potpie", "cherry", "buns", "dog" };
    [SerializeField] private string[] level2Passwords = { "ravioli", "hamburger", "lemonade", "apparatus", "chinese" };
    [SerializeField] private string[] level3Passwords = { "disestablishmentarianism", "unpalatable", "esoteric", "tnetennba", "creme fraiche" };

    //Game State
    private int _level;
    private enum Screen { MainMenu, Password, Win };
    private Screen _currentScreen;
    private string _password;


    // Start is called before the first frame update
    void Start()
    {
        ShowMainMenu(_greeting);
    }

    private void ShowMainMenu(string greeting)
    {
        _currentScreen = Screen.MainMenu;
        Terminal.ClearScreen();
        Terminal.WriteLine(greeting);
        Terminal.WriteLine("What would you like to hack into?");
        Terminal.WriteLine("");
        Terminal.WriteLine("Press 1 for facebook");
        Terminal.WriteLine("Press 2 for HMRC");
        Terminal.WriteLine("Press 3 for MI6");
        Terminal.WriteLine("");
        Terminal.WriteLine("Enter your selection: ");
    }

    private void OnUserInput(string input)
    {
        if (input == "menu")    //can always go to menu
        {
            //_currentScreen = Screen.MainMenu;
            ShowMainMenu(_greeting);
        }
        else if (input == "exit" || input == "quit")
        {
            Application.Quit();
        }
        else if (_currentScreen == Screen.MainMenu)
        {
            RunMainMenu(input);
        }
        else if (_currentScreen == Screen.Password)
        {
            RunPasswordScreen(input);
        }
    }

    private void RunMainMenu(string input)
    {
        bool isValidLevelNumber = (input == "1" || input == "2" || input == "3");
        if (isValidLevelNumber)
        {
            _level = int.Parse(input);
            AskForPassword();
        }
        else
        {
            Terminal.WriteLine("enter a valid level");
        }
    }

    private void AskForPassword()
    {
        _currentScreen = Screen.Password;
        Terminal.ClearScreen();
        SetRandomPassword();
        Terminal.WriteLine("Enter your password, hint: " + _password.Anagram());
    }

    private void SetRandomPassword()
    {
        switch (_level)
        {
            case 1:
                int index1 = Random.Range(0, level1Passwords.Length);
                _password = level1Passwords[index1];
                break;
            case 2:
                int index2 = Random.Range(0, level2Passwords.Length);
                _password = level2Passwords[index2];
                break;
            case 3:
                int index3 = Random.Range(0, level3Passwords.Length);
                _password = level3Passwords[index3];
                break;
            default:
                Debug.LogError("Invalid level number");
                break;
        }
    }

    private void RunPasswordScreen(string input)
    {
        if (input == _password)
        {
            DisplayWinScreen();
        }
        else
        {
            AskForPassword();
        }
    }

    private void DisplayWinScreen()
    {
        _currentScreen = Screen.Win;
        Terminal.ClearScreen();
        ShowLevelReward();
    }

    private void ShowLevelReward()
    {
        switch (_level)
        {
            case 1:
                Terminal.WriteLine("You guessed correctly, have a friend");
                Terminal.WriteLine(@"

0   0
 \_/

"
                );
                break;
            case 2:
                Terminal.WriteLine("Your tax has been made null");
                Terminal.WriteLine(@"

££££££££

"
                );
                break;
            case 3:
                Terminal.WriteLine("You are a special agent now");
                Terminal.WriteLine(@"

Welcome 007

"
                );
                break;
            default:
                Debug.LogError("Invalid level");
                break;
        }

        Terminal.WriteLine(_menuHint);

    }
}
